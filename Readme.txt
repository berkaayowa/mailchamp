- Refferences
- In your c# project add MailChimp.Net.V3 refference using nuget
- add refference to library MailchampCampaign.dll from MailchampCampaign project

Project
--------------------
-Library project name: MailchampCampaign
- Test project Name: Mailchamp

Usage
---------------------
in Mailchamp\Mailchamp\Program.cs are all function calls

Function cals
--------------------

            // mailchamp api key
            var mailchamp = new ExentedMailchimp.Helper("b126e04c987551664351ca90c51e1c49-us15");

            //Sending welcome message
            ExentedMailchimp.User user = new ExentedMailchimp.User();
            user.Name = "test";
            user.Surname = "some";
            user.Note = "welcome";
            user.EmailAddress = "ayowaberka@gmail.com";

            if (!mailchamp.Welcome(user))
            {
                //log error
            }

            //Sending Invite to care giver
            ExentedMailchimp.User careGiver = new ExentedMailchimp.User();
            careGiver.Name = "test";
            careGiver.Surname = "some";
            careGiver.Note = "Care giver";
            careGiver.EmailAddress = "ayowaberka@gmail.com";

            if (!mailchamp.InviteCareGiver(user))
            {
                //log error
            }

            //Sending Invite to family
            ExentedMailchimp.User userToInvite = new ExentedMailchimp.User();
            userToInvite.Name = "test";
            userToInvite.Surname = "some";
            userToInvite.Note = "Family";
            userToInvite.EmailAddress = "ayowaberka@gmail.com";

            if (!mailchamp.WelcomeFamilly(user))
            {
                //log error
            }
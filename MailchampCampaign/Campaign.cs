﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExentedMailchimp
{
    public class Campaign
    {
        public string EmailFromName { get; set; }
        public string EmailReplyTo { get; set; }
        public string EmailSubject { get; set; }
        public string Language { get; set; }
        public string TemplateName { get; set; }
    }
}

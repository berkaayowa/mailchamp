﻿using MailChimp.Net;
using MailChimp.Net.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExentedMailchimp
{
    public class Helper
    {

        private IMailChimpManager Manager = null;

        public Helper(string apiKey)
        {
            this.Api = apiKey;
            this.Manager = new MailChimpManager(apiKey);
        }

        public string Api
        {
            get; private set;
        }

        public int CreateUsersAndAddToAudience(List<User> users, string audienceName)
        {

            int memberCount = 0;

            var list = Manager.Lists.GetAllAsync().Result.FirstOrDefault(x => x.Name == audienceName);

            if (!string.IsNullOrEmpty(list.Id))
            {
                foreach (var user in users)
                {
                    var member = new MailChimp.Net.Models.Member { EmailAddress = user.EmailAddress, StatusIfNew = MailChimp.Net.Models.Status.Subscribed };
                    member.MergeFields.Add("FNAME", user.Name);
                    member.MergeFields.Add("LNAME", user.Surname);

                    if (!string.IsNullOrEmpty(user.Note))
                    {
                        member.LastNote = new MailChimp.Net.Models.MemberLastNote { Body = user.Note, CreatedBy = "C# libray" };
                        member.Tags = new List<MailChimp.Net.Models.MemberTag> {
                        new MailChimp.Net.Models.MemberTag{ Name = user.Note}
                    };
                    }

                    member = Manager.Members.AddOrUpdateAsync(list.Id, member).Result;

                    if (!string.IsNullOrEmpty(member.Id))
                    {
                        memberCount++;
                    }

                }
            }

            return memberCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user">User object to be added to an audience </param>
        /// <param name="audienceName">optional Name of the audience to add user to default audience is welcome</param>
        /// <returns></returns>
        public bool Welcome(User user, string audienceName = "welcome")
        {
            return CreateUsersAndAddToAudience(new List<User> { user }, audienceName) > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user">User object to be added to an audience </param>
        /// <param name="audienceName">optional Name of the audience to add user to default audience is caregiver</param>
        /// <returns></returns>
        public bool InviteCareGiver(User user, string audienceName = "caregiver")
        {
            return CreateUsersAndAddToAudience(new List<User> { user }, audienceName) > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user">User object to be added to an audience </param>
        /// <param name="audienceName">optional Name of the audience to add user to default audience is family</param>
        /// <returns></returns>
        public bool WelcomeFamilly(User user, string audienceName = "family")
        {
            return CreateUsersAndAddToAudience(new List<User> { user }, audienceName) > 0;
        }

    }
}
